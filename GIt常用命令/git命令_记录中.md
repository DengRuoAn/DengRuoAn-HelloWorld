># Git推送代码的主流程

简介：git管理代码，反正两个字牛x

>### 1.推送代码流程：

### 1.先git clone xxxx某请求代码

### 2.进入clone下的代码目录（创建并切换到该分支）

`git checkout -b xxx`

### 3.在目录中进行代码修改，修改完操作如下

* `git add .`
* `git commit -m "" 备注信息`
* `git pull origin master:wdeng --->拉取master更新到本地`（同步主分支内容可选操作）
* `git push  或 git push --set-upstream origin wdeng---->更新到指定分支`





>### 2.常用命令及问题记录

* `git clone -b 分支名 url		克隆指定分支  ----->常用`
* `git branch  --->查看当前分支`
* `git checkout -b wdeng --->创建分支并切换到当前分支(本地分支)`
* `git pull origin master:wdeng --->拉取master更新到本地`
* `git diff 文件---->查看文件的区别`
* `git reset HEAD---->删除git add。 提交的文件`
* `git diff --cached --->add后看区别`
* `git push --set-upstream origin wdeng---->更新到指定分支`
* `git push origin -d/--delete <分支名>----->删除远程分支`






#### 1.git版本回归

* git reflog
* git reset --hard xxx回归版本号





#### 2.git commit后如何取消？

**步骤**

* git add . //添加所有文件

* git commit -m "xxx"

**执行完commit后，想撤回commit？**
**解决方法:撤回还没有push的命令**
   `git reset --soft HEAD^`

