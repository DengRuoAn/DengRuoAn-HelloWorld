var ksdYuJu = ["但行好事，莫问前程", "去更远的地方，见更亮的光", "永远相信美好的事情即将发生", "生活原本沉闷，但跑起来就有风", "我们的征途是星辰大海", "保持热爱、奔赴山海", "须知少年凌云志，曾许人间第一流", "唯有热爱，能抵岁月漫长", "生活明朗，万物可爱，人间值得，未来可期", "一群有情有义的人、在一起做一些有价值有意义的事情", "即使再小的帆也能远航", "人就这么一辈子，开心也是一天，不开心也是一天，所以你一定要开心", "我们缺乏的不是知识，而是学而不厌的态度", "世界上只有少数人能够最终达到自己的理想", "生活是属于每个人自己的感受，不属于任何别人的看法", "受了这些苦，一定是为了什么值得的东西", "希望有一天自己也成为一个小太阳去温暖别人", "在谷底也要开花", "你若盛开，蝴蝶自来", "走自己的路，为自己的梦想去奋斗", "向着月亮出发，即使不能到达，也能站着群星之中", "真正喜欢的人和事，都值得我们坚持", "你最可爱，我说时来不及思索，但思索之后，还是这样说", "屏幕前的你一定是个很温柔的人吧", "别人拥有的，不必羡慕；只要努力，时间都会给你", "你只有走完必须走的路，才能过想过的生活", "凡是不能杀死你的，最终都会让你更强", "拥有希望的人，和漫天的星星一样，是永远不会孤独的", "失败，是正因你在距成功一步之遥的时候停住了脚步"];
function  changeYulumain(){
    var text = ksdYuJu[Math.floor(Math.random() * ksdYuJu.length)];
    $(".ksd-product-desctext").text(text);
}

function  changeYulu(){
    if($("body").data("font")) {
        var text = ksdYuJu[Math.floor(Math.random() * ksdYuJu.length)];
        var arr = [];
        for (var i = 0; i < text.length; i++) {
            arr.push("<span style='font-size: " + getRandomrange(12, 24) + "px'>" + text.charAt(i) + "</span>");
        }
        $(".ksd-product-desctext").html(arr.join(""));
    }else{
        changeYulumain();
    }
}

changeYulu();