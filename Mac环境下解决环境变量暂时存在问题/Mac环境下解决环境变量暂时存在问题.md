># Mac环境下解决环境变量暂时存在问题（环境变量变永久）

### 例如：`mysql`

- Mac配置mysql提示zsh: command not found: mysql，若输入mysql -u root -p，出现：zsh: command not found: mysql的提示


### 解决方案：

1. `cd /usr/local/bin`       //进入添加脚本目录
2. `sudo ln -fs /usr/local/mysql-8.0.25-macos11-x86_64/bin/mysql mysql`   //加入脚本




### 环境变量仅生成一次的问题：
解决方法：
1.创建`.zshrc文件`（如果已经有了就无须创建）
  `$ touch .zshrc`



2.编辑`.zshrc`文件
   `open .zshrc`



3.向`.zshrc`中添加环境变量
  `export PATH=xxxx你想配置生效的路径:$PATH`



4.刷新配置文件使其生效
  `source ~/.bash_profile`   
