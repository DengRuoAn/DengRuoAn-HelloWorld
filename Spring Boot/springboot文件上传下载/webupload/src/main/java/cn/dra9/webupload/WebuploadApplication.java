package cn.dra9.webupload;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author dengruoan
 */
@SpringBootApplication
public class WebuploadApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebuploadApplication.class, args);
    }

}
