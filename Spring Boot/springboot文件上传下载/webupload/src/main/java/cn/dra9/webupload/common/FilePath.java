package cn.dra9.webupload.common;

import java.io.File;

/**
 * @Author dengruoan
 * @Date 2022/4/3 下午8:28
 * @Description
 * @Version 1.0
 * @email dwwen1126@163.com
 */
public class FilePath {
    /**当前项目目录*/
    public static final String FILE_PATH = System.getProperty("user.dir");
    /**临时存储目录*/
    public static final String TEMP_FILE_PATH = FILE_PATH + File.separator + "temp";
    /**真实下载路径*/
    public static final String REL_FILE_PATH = FILE_PATH + File.separator + "file";
}
