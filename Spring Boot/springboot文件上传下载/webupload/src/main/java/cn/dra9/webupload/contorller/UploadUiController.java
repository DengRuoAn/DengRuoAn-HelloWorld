package cn.dra9.webupload.contorller;

import cn.dra9.webupload.common.FilePath;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;


/**
 * @Author dengruoan
 * @Date 2022/4/3 下午8:26
 * @Description
 * @Version 1.0
 * @email dwwen1126@163.com
 */
@Controller
public class UploadUiController {
    /**
     * 跳转到首页
     *
     * @return
     */
    @GetMapping("index")
    public String toUpload(Model model) {
        model.addAttribute("uploadPath", FilePath.REL_FILE_PATH);
        return "upload";
    }
}
