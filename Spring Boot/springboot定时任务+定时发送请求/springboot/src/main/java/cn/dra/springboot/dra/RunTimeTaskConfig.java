package cn.dra.springboot.dra;

import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;

/**
 * @Author dengruoan
 * @Date 2021/7/31 10:38
 * @Version 1.0
 */
@Slf4j
public class RunTimeTaskConfig {

    public static void runTimeTask(String url , String message ,int year ,int month,int day,int hours,int minutes ,int milliseconds) {
        try {
            //调度任务
            Timer timer = new Timer();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            //1。设置当前时间
            //Calendar.getInstance()直接获取当前时间对象
            Calendar c1 = Calendar.getInstance();
            c1.setTime(new Date());
            log.info("当前时间：" + sdf.format(c1.getTimeInMillis()));

            //2.设置任务时间
            Calendar c2 = Calendar.getInstance();
            c2.set(year,month-1,day,hours,minutes,milliseconds);
//            c2.set(Calendar.YEAR, year);
//            c2.set(Calendar.MONTH, month-1);
//            c2.set(Calendar.DAY_OF_MONTH, day);
//            c2.set(Calendar.HOUR, hours);
//            c2.set(Calendar.MINUTE, minutes);
//            c2.set(Calendar.SECOND, milliseconds);
            log.info("任务时间：" + sdf.format(c2.getTimeInMillis()));

            //3。设置触发的时间
            long diffs = c2.getTimeInMillis() - c1.getTimeInMillis();
            log.info("diffs：" + diffs);

            //new一个自己任务对象
            MyTask task = new MyTask();
            task.setUrl(url);
            task.setMessage(message);
            //使用time，只执行一次
            timer.schedule(task, diffs);

            //设置每8s执行一次
            //timer.scheduleAtFixedRate(task, diffs,8000);
        } catch (Exception e) {
            try {
                throw new Exception("当前时间设置错误");
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }

    }
}
