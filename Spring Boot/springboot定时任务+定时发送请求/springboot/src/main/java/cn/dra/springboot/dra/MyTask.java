package cn.dra.springboot.dra;

import cn.hutool.core.date.DateTime;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimerTask;

/**
 * @Author dengruoan
 * @Date 2021/7/31 09:03
 * @Version 1.0
 * //        //发送 GET 请求
 * //        String s = HttpRequest.sendGet("http://localhost:8080/hello", "key=dra&value=1126");
 * //        System.out.println("发送 GET 请求：" + s);
 * //
 * //        //发送 POST 请求
 * //        String sr = HttpRequest.sendPost("http://localhost:8080/hello", "key=dra&value=1126");
 * //        System.out.println("发送 POST 请求返回值：" + sr);
 * <p>
 * 执行自己需要执行的任务
 */

public class MyTask extends TimerTask {
    private int i = 1;

    private String url ;
    private String message ;

    public void setUrl(String url) {
        this.url = url;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public void run() {
//        Calendar c = Calendar.getInstance();
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        System.out.println("当前时间：" + sdf.format(c.getTime()));
        System.out.println("当前时间：" + DateTime.now());
        System.out.println("第" + i + "次执行任务开始");

        String s = HttpRequestUtil.sendPost(url, "message="+message);

        System.out.println("应答信息：" + s);
        System.out.println("第" + i + "次执行任务结束");

    }


}
