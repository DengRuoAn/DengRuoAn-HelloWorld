package cn.dra.springboot.dra;

import lombok.extern.slf4j.Slf4j;



/**
 * @Author dengruoan
 * @Date 2021/7/31 09:04
 * @Version 1.0
 * 测试主程序
 */
@Slf4j
public class Run {
    public static void main(String[] args) {
        //传入时间 定时发送
        RunTimeTaskConfig.runTimeTask("http://localhost:8080/hello","你好啊",2021, 8, 2, 9, 49, 00);
    }



}
