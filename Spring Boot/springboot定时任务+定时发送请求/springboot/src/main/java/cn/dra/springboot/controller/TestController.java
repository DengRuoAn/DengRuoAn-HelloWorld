package cn.dra.springboot.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author dengruoan
 * @Date 2021/7/18 16:48
 * @Version 1.0
 */
@Slf4j
@Controller
public class TestController {

    /**
     * 测试环境是否正常请求
     * @param model
     * @return
     */
    @GetMapping("/dra")
    public String getDra(Model model) {
        model.addAttribute("msg", "ni hao  dengruoan");
        return "hello";
    }


    /**
     * 测试定时发送接口的接收接口
     * @param message
     * @return
     */
    @RequestMapping("/hello")
    public String getHello(String message) {
        log.info(message);
        return message;
    }
}
