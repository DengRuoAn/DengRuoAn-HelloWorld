># Mybatis-plus实现CRUD及分页功能----springboot



>## 1.导入依赖包（maven的pom文件）

```xml
<!-- mybatis-plus   -->
    <dependency>
      <groupId>com.baomidou</groupId>
      <artifactId>mybatis-plus-boot-starter</artifactId>
      <version>3.4.3</version>
    </dependency>
    <dependency>
      <groupId>com.baomidou</groupId>
      <artifactId>mybatis-plus-generator</artifactId>
      <version>3.4.1</version>
    </dependency>
    <dependency>
      <groupId>com.baomidou</groupId>
      <artifactId>mybatis-plus-extension</artifactId>
      <version>3.4.3</version>
    </dependency>

```

>## 2.application.yml配置文件配置如下

```yml
server:
  port: 8080
spring:
  datasource:
  	#8.0以上数据库用com.mysql.cj.jdbc.Driver
    driver-class-name: com.mysql.jdbc.Driver
    url: jdbc:mysql://127.0.0.1:3306/xxx?useUnicode=true&characterEncoding=utf8&allowMultiQueries=true&useSSL=false
    username: root
    password: root
    type: com.alibaba.druid.pool.DruidDataSource
    #配置数据库连接池
    druid:
    #统计监控和防火墙监控
      filters: stat,wall
      #最大连接池数量
      max-active: 200
      #初始化时建立物理连接的个数
      initial-size: 10
      #获取连接时最大等待时间，单位毫秒
      max-wait: 60000
      #最小连接池数量
      min-idle: 10
      time-between-eviction-runs-millis: 60000
      min-evictable-idle-time-millis: 300000
      validation-query: select 1
      #validation-query: select 1 from dual
      test-while-idle: true
      test-on-borrow: false
      test-on-return: false
      #是否缓存preparedStatement
      pool-prepared-statements: true
      #要启用PSCache，必须配置大于0
      max-open-prepared-statements: 200
      break-after-acquire-failure: true
      time-between-connect-error-millis: 300000
      # 下面3个enabled改为true即开启druid monitor
      filter:
        config:
          enabled: true
      # 配置 DruidStatFilter
      web-stat-filter:
        enabled: true
        url-pattern: /*
        exclusions: .js,*.gif,*.jpg,*.bmp,*.png,*.css,*.ico,/druid/*

      # 配置 DruidStatViewServlet
      stat-view-servlet:
        enabled: true
        url-pattern: /druid/*
        # IP 白名单，没有配置或者为空，则允许所有访问
        allow:
        # IP 黑名单，若白名单也存在，则优先使用
        deny:
        # 禁用 HTML 中 Reset All 按钮
        reset-enable: true
        # druid登录用户名/密码
        login-username: admin
        login-password: admin
  #前端页面thymeleaf模板   
  thymeleaf:
    suffix: .html
    prefix: classpath:/templates/
#mybatis:
#  config-location: classpath:/mapper/*.xml

mybatis-plus:
  #扫描mapper的xml文件（xml里面可以直接写CRUD操作数据库）
  mapper-locations: classpath:/mapper/*.xml
  configuration:
    #使用 log-impl使控制台打印出信息
    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl


```

>## 3.Dao层接口

```java
package cn.dra.dao;

import cn.dra.pojo.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author dra
 * @since 2021-08-26
 *
 *
 * 1.这是一个mapper接口类需要添加上@Mapper注解
 * 2.继承BaseMapper<返回值类型>    [比如当前你操作的是user实体类，想返回user对象操作] BaseMapper里面封装了大量的CRUD的方法
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
  /**
   * 1.IPage分页 Page 对象接口
   * 2.IPage<分页对象>
   * 3.selectPage(Page page);   Page是简单分页模型 [里面有 查询数据列表  总数  每页显示条数，默认 10  当前页 等]
   * @param page
   * @return
   */
  IPage<User> selectPage(Page page);
}

```

![image-20210906110834812](mybatis-plus%E5%AE%9E%E7%8E%B0%E5%88%86%E9%A1%B5%E5%8A%9F%E8%83%BD.assets/image-20210906110834812.png)



>## 4.Service层接口

```java
package cn.dra.service;

import cn.dra.pojo.User;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dra
 * @since 2021-08-26
 *
 * 1.UserService继承mybatis-plus的IService<返回值类型>
 * 2.继承后有大量的CRUD可以进行使用
 */
public interface UserService extends IService<User> {

  /**
   * 1.IPage分页 Page 对象接口
   * 2.IPage<分页对象>
   * 3.selectPage(Page page);   Page是简单分页模型 [里面有 查询数据列表  总数  每页显示条数，默认 10  当前页 等]
   * @param page
   * @return
   */
  IPage<User> selectPage(Page page);

}

```

![image-20210906111252288](mybatis-plus%E5%AE%9E%E7%8E%B0%E5%88%86%E9%A1%B5%E5%8A%9F%E8%83%BD.assets/image-20210906111252288.png)



>## 5.Service的impl实现类层

```java
package cn.dra.service.impl;

import cn.dra.dao.UserMapper;
import cn.dra.pojo.User;
import cn.dra.service.UserService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dra
 * @since 2021-08-26
 *
 * 1.UserServiceImpl extends ServiceImpl<daoMapper接口, 返回值类型>
 * 2.UserServiceImpl 继承mybatis-plus 的 IService 实现类（ 泛型：M 是 mapper 对象，T 是实体 ）可以操作大量的CRUD
 *
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

  /**
   * @Resource注入 UserMapper层接口进行数据库操作
   */
  @Resource
  UserMapper userMapper;

  /**
   * 自己的分页查询user
   * @param page
   * @return
   */
  @Override
  public IPage<User> selectPage(Page page) {
    return  userMapper.selectPage(page);
  }
}

```

![image-20210906112139341](mybatis-plus%E5%AE%9E%E7%8E%B0%E5%88%86%E9%A1%B5%E5%8A%9F%E8%83%BD.assets/image-20210906112139341.png)



>## 6.mapper层的xml文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<!--查询所有数据，自动实现分页-->
<mapper namespace="cn.dra.dao.UserMapper">
  <select id="selectPage" resultType="cn.dra.pojo.User">
     SELECT * FROM user
  </select>

</mapper>

```

![image-20210906112313517](mybatis-plus%E5%AE%9E%E7%8E%B0%E5%88%86%E9%A1%B5%E5%8A%9F%E8%83%BD.assets/image-20210906112313517.png)







>## 7.UserController处理数据层

```java
package cn.dra.controller;

import cn.dra.pojo.User;
import cn.dra.service.UserService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @description:
 * @date: 2021/8/26 下午1:42
 * @author: RuoAn
 * @version: 1.0
 */
@Controller
public class UserController {

  @Autowired
  UserService userService;


  @GetMapping("/test")
  public String toTestHtml(Model model) {
    Page<User> userPage = new Page<User>(2, 2); //当前页和每页显示大小设置
    IPage<User> users = userService.selectPage(userPage);
    model.addAttribute("users",users);
    return "test";
  }

}

```

>## 8.HTML页面的编写

```html
<!DOCTYPE html>
<html  xmlns:th="http://www.thymeleaf.org">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
</head>
<body>
<h1>User数据</h1>
<h1 >当前页:[[${users.getCurrent()}]]</h1>
<h1>总记录数:[[${users.getTotal()}]]</h1>
<h1>每页显示[[${users.getSize()}]]条</h1>
<h1>共[[${users.getPages()}]]页</h1>
<h1 th:text="${users.getRecords()}">test</h1>   //所有查询的user的数据
  <li>
    <table border="1">
      <tr>
<!--        <th>index</th>-->
        <th>序号</th>
        <th>用户名</th>
        <th>密码</th>
        <th>角色名称</th>
        <th>ACL</th>
        <th>usb权限</th>
        <th>快照权限</th>
        <th>主虚拟机 ID</th>
        <th>视频录制</th>
        <th>terminoEcho</th>
      </tr>
      <tr  th:each="user,index : ${users.getRecords()}">
<!--        <td th:text="${index.count}">Onions</td>-->   //前端自带序号
        <td th:text="${user.uid}">Onions</td>
        <td th:text="${user.username}">test@test.com.cn</td>
        <td th:text="${user.password}">yes</td>
        <td th:text="${user.rolename}">yes</td>
        <td th:text="${user.acl == null ? 'dra' : 'dengruoan'}"></td>
        <td th:text="${user.usbpermission}">yes</td>
        <td th:text="${user.snapshotpermission}">yes</td>
        <td th:text="${user.masterVmId}">yes</td>
        <td th:text="${user.videoRecording}">yes</td>
        <td th:text="${user.terminoEcho}">yes</td>
      </tr>
    </table>
  </li>
</body>
</html>

```

### `页面效果：`

![image-20210906113126358](mybatis-plus%E5%AE%9E%E7%8E%B0%E5%88%86%E9%A1%B5%E5%8A%9F%E8%83%BD.assets/image-20210906113126358.png)