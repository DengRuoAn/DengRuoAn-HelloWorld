---
home: true
heroImage: /image/dra1.PNG
heroText: 邓若安
tagline: 我一定要在黄昏前到达
actionText: 开始阅读 →
actionLink: /dra/
features:
  - title: 生于尘埃
    details: 熬过无人问津的日子才有诗和远方
  - title: 溺于人海
    details: 穷且益坚,不坠青云之志,老当益壮,宁移白首之心。
  - title: 死于理想
    details: 天将降大任于是人也，必先苦其心志，劳其筋骨，饿其体肤，空乏其身，行拂乱其所为。
footer: 皖ICP备20013878号 | Copyright © 2022 邓若安
---