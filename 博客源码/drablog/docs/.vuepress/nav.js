const navs = [
    {
      text: '首页',
      link: '/',
    },
  
    {
      text: '前端',
      items: [
        { text: "vue", link: "/web/vue/" },
        { text: "css", link: "/web/css/" },
      ],
    },
    { text: '后端', link: '/java/' },
    { text: '进阶', link: '/advanced/' },
    { text: '关于', link: '/about/' },
    { text: '网站', link: 'http://www.dra9.cn' }
  ];
  module.exports = navs;
