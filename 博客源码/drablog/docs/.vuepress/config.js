module.exports = {
  base: '/dengruoan/', //设置站点根路径

  plugins: ['@vuepress/back-to-top', [
    //先安装在配置， npm install @vuepress-plugin-meting --save
    //https://api.mizore.cn/meting/?server=netease&type=playlist&id=2583073630
    'meting', {
      meting: {
        //metingApi: 'https://api.mizore.cn/meting/',
        server: "netease",
        type: "playlist",
        mid: "2583073630"
      },
      // 不配置该项的话不会出现全局播放器
      aplayer: {
        lrcType: 3
      }
    }
  ]
  // [
  //   //彩带背景 先安装在配置， npm install vuepress-plugin-ribbon --save
  //   "ribbon",
  //   {
  //     size: 90,     // width of the ribbon, default: 90
  //     opacity: 0.8, // opacity of the ribbon, default: 0.3
  //     zIndex: -1    // z-index property of the background, default: -1
  //   }
  // ]
  ],

  head: [
    [
      'link', // 设置 favicon.ico，注意图片放在 public 文件夹下
      { rel: 'icon', href: 'logo.png' }
    ]
  ],
  title: '邓若安',
  description: '从现在开始，不留余力地努力吧，最差的结果，也不过是大器晚成',
  themeConfig: {
    sidebar: 'auto', //自动获取侧边栏内容
    displayAllHeaders: true,
    markdown: {
      extractHeaders: ['h2', 'h3', 'h4', 'h5', 'h6'],
    },

    //设置主页点击logo
    logo: '/logo.png',
    //nav是标签栏
    nav: [
      { text: '首页', link: '/' },
      // 带下拉框的前端
      {
        text: '前端',
        items: [
          { text: "vue", link: "/web/vue/" },
          { text: "css", link: "/web/css/" },
        ]
      },
      {
        text: '后端',
        items: [
          { text: "Git", link: "/java/git/" },
          { text: "java", link: "/java/java/" },
        ]
      },
      { text: '进阶', link: '/advanced/' },
      { text: '关于', link: '/about/' },
      { text: '网站', link: 'http://www.dra9.cn' }
    ]
  }
}

